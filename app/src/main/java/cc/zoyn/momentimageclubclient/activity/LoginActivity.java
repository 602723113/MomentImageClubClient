package cc.zoyn.momentimageclubclient.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.widget.CompoundButton;
import cc.zoyn.momentimageclubclient.R;
import net.qiujuer.genius.ui.widget.Button;
import net.qiujuer.genius.ui.widget.CheckBox;
import net.qiujuer.genius.ui.widget.EditText;

/**
 * 注册界面
 *
 * @author Zoyn
 * @since 2017-10-07
 */
public class LoginActivity extends AppCompatActivity {

    private EditText accountEditText;
    private EditText passwordEditText;
    private CheckBox checkBoxDisplayPassword;
    private Button registerButton;
    private Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        findViews();
        initialCheckBoxListener();
    }

    private void initialCheckBoxListener() {
        checkBoxDisplayPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //选择状态 显示明文--设置为可见的密码
                    passwordEditText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                } else {
                    //默认状态显示密码--设置文本 要一起写才能起作用 InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD
                    passwordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });
    }

    private void findViews() {
        accountEditText = (EditText) findViewById(R.id.layout_login_accountEditText);
        passwordEditText = (EditText) findViewById(R.id.layout_login_passwordEditText);
        checkBoxDisplayPassword = (CheckBox) findViewById(R.id.layout_login_checkBox);
        registerButton = (Button) findViewById(R.id.layout_login_registerButton);
        loginButton = (Button) findViewById(R.id.layout_login_loginButton);
    }
}

