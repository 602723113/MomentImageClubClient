package cc.zoyn.momentimageclubclient.activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import cc.zoyn.momentimageclubclient.R;

/**
 * 设置界面
 *
 * @author Zoyn
 * @since 2017-09-26
 */
public class SettingsActivity extends AppCompatActivity {

    private static SettingsActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        getFragmentManager().beginTransaction().replace(android.R.id.content, new PrefsFragment()).commit();

        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
        layoutParams.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
        getWindow().setAttributes(layoutParams);
    }

    public static class PrefsFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);
        }

        // 点击设置的监听
        @Override
        public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
            if (preference.getTitle().equals("关于")) {
                Intent intent = new Intent(instance, AboutActivity.class);
                startActivity(intent);
            }
            return false;
        }
    }
}
