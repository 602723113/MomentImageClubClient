package cc.zoyn.momentimageclubclient.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.DrawableUtils;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.*;
import cc.zoyn.momentimageclubclient.R;
import cc.zoyn.momentimageclubclient.builder.ToastBuilder;
import cc.zoyn.momentimageclubclient.fragment.MainFragment;
import cc.zoyn.momentimageclubclient.fragment.SchoolFragment;
import cc.zoyn.momentimageclubclient.util.NetworkUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private long exitTime;
    private boolean isOpen = false;

    //声明相关变量
    private Toolbar toolbar;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    //    private ListView lvLeftMenu;
    private String[] content = {"用户中心", "设置"};
    private SimpleAdapter simpleAdapter;
    private List<Map<String, Object>> leftMenuData;
    private ArrayAdapter arrayAdapter;

    private ImageView takePhotoImage;
    private ImageView schoolImage;
    private ImageView labImage;

    private MainFragment mainFragment;
    private SchoolFragment schoolFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViews(); //获取控件
        toolbar.setTitle(" ");
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_menu_user);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true); //设置返回键可用
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        //创建返回键，并实现打开关/闭监听
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open, R.string.close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                isOpen = true;
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                isOpen = false;
            }
        };
        mDrawerToggle.syncState();
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        Button button = (Button) findViewById(R.id.layout_drawer_relative_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!NetworkUtils.hasWebConnected()) {
                    ToastBuilder.createToast(MainActivity.this, Toast.LENGTH_SHORT)
                            .setMessage("未检测到网络连接!")
                            .setBackgroundColor(Color.YELLOW)
                            .setMessageColor(Color.BLACK)
                            .show();
                }
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        // 初始化Fragment
        initialFragment();
        takePhotoImage.setOnClickListener(this);
        schoolImage.setOnClickListener(this);
        labImage.setOnClickListener(this);


        //设置菜单列表
//        arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, content);
//        lvLeftMenu.setAdapter(arrayAdapter);
//
//        lvLeftMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
//                String selectItem = lvLeftMenu.getItemAtPosition(position).toString();
//                if (selectItem.equals("用户中心")) {
//                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
//                    startActivity(intent);
//                } else if (selectItem.equals("设置")) {
//                    Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
//                    startActivity(intent);
//                }
//            }
//        });

    }

    @SuppressLint("NewApi")
    private void initialFragment() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        mainFragment = new MainFragment();
        transaction.add(mainFragment, "homepage").add(schoolFragment, "school");
        transaction.show(mainFragment).hide(schoolFragment).commit();
    }

    @SuppressLint("NewApi")
    @Override
    public void onClick(View v) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        switch (v.getId()) {
            case R.id.layout_bottom_bar_homepage:
                if (mainFragment == null) {
                    mainFragment = new MainFragment();
                }

                transaction.replace(R.id.id_content, mainFragment);
                break;

            case R.id.layout_bottom_bar_take_photo:
                if (mainFragment == null) {
                    mainFragment = new MainFragment();
                }

                transaction.replace(R.id.id_content, mainFragment);
                break;

            case R.id.layout_bottom_bar_school:
                if (schoolFragment == null) {
                    schoolFragment = new SchoolFragment();
                }

                transaction.replace(R.id.id_content, schoolFragment);
                ToastBuilder.createToast(this)
                        .setDuration(Toast.LENGTH_SHORT)
                        .setMessageColor(Color.GREEN)
                        .setBackgroundColor(Color.RED)
                        .setMessage("点击了School")
                        .show();
                break;
        }
        // transaction.addToBackStack();
        // 事务提交
        transaction.commit();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (!isOpen) {
                if ((System.currentTimeMillis() - exitTime) > 2000) {
                    Toast.makeText(this, "再按一次退出客户端", Toast.LENGTH_SHORT).show();
                    exitTime = System.currentTimeMillis();
                } else {
                    System.exit(0);
                }
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void findViews() {
        toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.lay_left);
//        lvLeftMenu = (ListView) findViewById(R.id.lv_left_menu);
//        bottomTabBar = (BottomTabBar) findViewById(R.id.bottom_tab_bar);
        takePhotoImage = (ImageView) findViewById(R.id.layout_bottom_bar_take_photo);
        schoolImage = (ImageView) findViewById(R.id.layout_bottom_bar_school);
        labImage = (ImageView) findViewById(R.id.layout_bottom_bar_lab);
    }

    private List<Map<String, Object>> loadLeftMenuData() {
        leftMenuData = Lists.newArrayList();
        Map<String, Object> map = Maps.newHashMap();
        map.put("image", R.drawable.ic_menu_user);
        map.put("title", "用户");
        leftMenuData.add(map);

        map = new HashMap<>();
        map.put("image", R.drawable.ic_menu_school);
        map.put("title", "学校");
        leftMenuData.add(map);
        return leftMenuData;
    }
}
