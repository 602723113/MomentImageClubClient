package cc.zoyn.momentimageclubclient.builder;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.TextViewCompat;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

/**
 * ToastBuilder - Builder
 *
 * @author Zoyn
 * @since 2017-10-12
 */
public class ToastBuilder {

    /**
     * 默认颜色
     */
    private static final int DEFAULT_COLOR = 0xffffffff;
    private static final int DEFAULT_DURATION = Toast.LENGTH_LONG;

    private Toast currentToast;
    private String message = "";
    private int duration = DEFAULT_DURATION;
    private int messageColor = DEFAULT_COLOR;
    private int backgroundColor = DEFAULT_COLOR;

    private ToastBuilder(Context context) {
        currentToast = Toast.makeText(context, this.message, duration);
    }

    private ToastBuilder(Context context, int duration) {
        this.duration = duration;
        currentToast = Toast.makeText(context, this.message, this.duration);
    }

    public static ToastBuilder createToast(Context context) {
        return new ToastBuilder(context);
    }

    public static ToastBuilder createToast(Context context, String message) {
        return new ToastBuilder(context).setMessage(message);
    }

    public static ToastBuilder createToast(Context context, int duration) {
        return new ToastBuilder(context, duration);
    }

    public static ToastBuilder createToast(Context context, String message, int duration) {
        return new ToastBuilder(context, duration).setMessage(message);
    }

    public String getMessage() {
        return message;
    }

    public ToastBuilder setMessage(String message) {
        this.message = message;
        return this;
    }

    public ToastBuilder setDuration(int duration) {
        this.duration = duration;
        return this;
    }

    public ToastBuilder setMessageColor(int messageColor) {
        this.messageColor = messageColor;
        return this;
    }

    public ToastBuilder setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
        return this;
    }

    public View getView() {
        return currentToast.getView();
    }

    public ToastBuilder setView(View view) {
        this.currentToast.setView(view);
        return this;
    }

    public void show() {
        // set text
        this.currentToast.setText(message);

        TextView textView = (TextView) this.currentToast.getView().findViewById(android.R.id.message);
        textView.setTextColor(this.messageColor);
        TextViewCompat.setTextAppearance(textView, android.R.style.TextAppearance);

        // set background
        View toastView = this.currentToast.getView();
        Drawable background = toastView.getBackground();
        background.setColorFilter(new PorterDuffColorFilter(this.backgroundColor, PorterDuff.Mode.SRC_IN));

        // set duration
        this.currentToast.setDuration(this.duration);

        // show toast
        this.currentToast.show();
    }

}
