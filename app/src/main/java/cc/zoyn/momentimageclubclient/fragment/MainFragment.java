package cc.zoyn.momentimageclubclient.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import cc.zoyn.momentimageclubclient.R;

/**
 * 主页 - Fragment
 *
 * @author Zoyn
 * @since 2017-09-28
 */
public class MainFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        TextView tv = (TextView) v.findViewById(R.id.fragment_main_textView);
        tv.setText("测试...");
        return v;
    }
}
