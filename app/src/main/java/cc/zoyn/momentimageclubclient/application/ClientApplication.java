package cc.zoyn.momentimageclubclient.application;

import android.app.Application;
import cc.zoyn.momentimageclubclient.handler.CrashHandler;
import cc.zoyn.momentimageclubclient.util.BaseUtils;

/**
 * 客户端Application
 *
 * @author Zoyn
 * @since 2017-10-06
 */
public class ClientApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        CrashHandler crashHandler = CrashHandler.getInstance();
        crashHandler.initial(getApplicationContext());

        BaseUtils.initial(this);
    }
}
