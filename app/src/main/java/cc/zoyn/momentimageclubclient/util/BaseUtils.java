package cc.zoyn.momentimageclubclient.util;

import android.annotation.SuppressLint;
import android.app.Application;

/**
 * 基础工具类
 *
 * @author Zoyn
 * @since 2017-10-12
 */
public class BaseUtils {

    /**
     * 应用程序对象
     */
    @SuppressLint("StaticFieldLeak")
    private static Application application;

    /**
     * 初始化工具类
     *
     * @param app 程序对象
     */
    public static void initial(Application app) {
        application = app;
    }

    public static Application getApplication() {
        if (application == null) {
            throw new NullPointerException("需要初始化Application引用!");
        }
        return application;
    }

}
