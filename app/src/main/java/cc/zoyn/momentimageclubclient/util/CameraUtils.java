package cc.zoyn.momentimageclubclient.util;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.File;

/**
 * 照相机工具类
 *
 * @author May_Speed
 * @since 2017-09-21
 */
public class CameraUtils {

    /**
     * 调用系统摄像头拍照
     *
     * @param activity    activity
     * @param path        保存路径
     * @param requestCode 请求参数
     */
    public static void takePhotoByMethod1(Activity activity, String path, int requestCode) {
        // 实例化intent,指向摄像头
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //根据路径实例化图片文件
        File photoFile = new File(path);
        //设置拍照后图片保存到文件中
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
        //启动拍照activity并获取返回数据
        activity.startActivityForResult(intent, requestCode);
    }

    /**
     * 调用系统摄像头拍照
     */
    public static void takePhotoBySystem(Activity activity, int requestCode) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        activity.startActivityForResult(intent, requestCode);
    }
}
