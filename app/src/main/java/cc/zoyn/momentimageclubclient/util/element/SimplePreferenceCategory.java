package cc.zoyn.momentimageclubclient.util.element;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

/**
 * @author Zoyn
 * @since 2017-09-26
 */
public class SimplePreferenceCategory extends PreferenceCategory {

    TextView textView;
    SharedPreferences preferences;

    public SimplePreferenceCategory(Context context, AttributeSet attrs) {
        super(context, attrs);
        preferences = PreferenceManager.getDefaultSharedPreferences(context);

    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);
        if (view instanceof TextView) {
            this.textView = (TextView) view;
        }
    }

    public TextView getTextView() {
        return textView;
    }
}
