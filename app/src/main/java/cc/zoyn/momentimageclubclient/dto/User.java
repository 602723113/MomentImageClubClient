package cc.zoyn.momentimageclubclient.dto;

/**
 * 用户 - 数据传输模型
 *
 * @author Zoyn
 * @since 2017-09-22
 */
public class User {

    private String displayName;
    private String account;
    private String password;
    private long createTime;

}
