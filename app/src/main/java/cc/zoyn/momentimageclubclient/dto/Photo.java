package cc.zoyn.momentimageclubclient.dto;

/**
 * 图片 - 数据传输模型
 *
 * @author Zoyn
 * @since 2017-09-22
 */
public class Photo {

    private String picture;
    private String author;

    public Photo(String picture, String author) {
        this.picture = picture;
        this.author = author;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
