package cc.zoyn.momentimageclubclient.dto;

/**
 * 网络类型
 *
 * @author Zoyn
 * @since 2017-10-12
 */
public enum NetworkType {

    NETWORK_WIFI, NETWORK_4G, NETWORK_3G, NETWORK_2G, UNKNOW;

}
